/**
 * 
 */
package com.invest.crypto.bittrex.config;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.common.util.ManagerBase;

/**
 * @author Tabakov
 *
 */
public class EnvironmentConfigManager extends ManagerBase {

	private static final Logger log = LogManager.getLogger(EnvironmentConfigManager.class);
	public static final long TIME_TO_WAIT_30_SEC = 30 * 1000;
	public static Map<String, String> configUrl = new HashMap<>();
	public static Map<String, String> availableCurrnecies = new HashMap<>();
	public static final String URL = "URL";

	public static Map<String, String> getBaseUrl() throws SQLException {

		if (configUrl.isEmpty()) {
			Connection con = null;
			try {
				con = getConnection();
				configUrl = EnvironmentConfigDAO.getBaseUrl(con);

			} finally {
				closeConnection(con);
			}
		}
		return configUrl;
	}

	public static long getTickerHashInterval() {
		Connection con = null;
		try {
			con = getConnection();
			return EnvironmentConfigDAO.getTickerHashInterval(con);
		} catch (SQLException e) {
			log.debug("Unable to get ticker hash interval from DB, so we set 30 sec interval by default");
			log.debug("Unable to get ticker hash interval", e);
			return TIME_TO_WAIT_30_SEC;
		} finally {
			closeConnection(con);
		}
	}
	
	public static long getTickerHashIntervalCoinMarketCap() {
		Connection con = null;
		try {
			con = getConnection();
			return EnvironmentConfigDAO.getTickerHashIntervalCoinMarketCap(con);
		} catch (SQLException e) {
			log.debug("Unable to get ticker hash interval from DB, so we set 30 sec interval by default");
			log.debug("Unable to get ticker hash interval", e);
			return TIME_TO_WAIT_30_SEC;
		} finally {
			closeConnection(con);
		}
	}
	
	public static Map<String, String> getAvailableCurrencies(){

		if (availableCurrnecies.isEmpty()) {
			Connection con = null;
				
				try {
					con = getConnection();
					availableCurrnecies = EnvironmentConfigDAO.getAvailableCurrencies(con);
				} catch (SQLException e) {
					log.error("coinmarketcap ticker is fail to get all available currenciess due to: ", e);
				}finally {
					closeConnection(con);
				}
		}
		return availableCurrnecies;
	}

}
