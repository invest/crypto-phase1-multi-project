/**
 * 
 */
package com.invest.crypto.coinmarketcap.api;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invest.crypto.bittrex.config.EnvironmentConfigManager;
import com.invest.crypto.bittrex.util.Util;
import com.invest.crypto.coinmarketcap.model.CoinMarket;

/**
 * @author Tabakov
 *
 */
@Path("/api")
public class CoinmarketcapAPI {
	
	private static final Logger log = LogManager.getLogger(CoinmarketcapAPI.class);
	private static final String BASE_URL = "https://api.coinmarketcap.com/v1/ticker/";
	
	private static Map<String, CoinMarket> cacheTickerCoinMarketCap = new HashMap<>();
	private static Map<LocalDateTime, Long> timeToWaitCoinMarketCap = new HashMap<>();
	private static final long ONE_HOUR = 1;

	@GET
	@Path("/ticker2")
	@Produces(MediaType.APPLICATION_JSON)
	public CoinMarket getTicker(@QueryParam("id") String id, @QueryParam("currency") String currency) throws JsonProcessingException, IOException {

		long serviceCallTime = System.currentTimeMillis();
		String currencyToLower = currency.toLowerCase();
		Map<String, String> avilableCurrencies = EnvironmentConfigManager.getAvailableCurrencies();
		CoinMarket ticker = new CoinMarket();
		String url = BASE_URL + id + "/?convert=" + currencyToLower;
		
		if(!avilableCurrencies.isEmpty() && avilableCurrencies.containsKey(currencyToLower)) {
			String priceCurrencyRequestedFieldName = "price_"+currencyToLower;
			String key = id+currencyToLower;
		

				log.debug("coinmarketcap ticker is called with params --> id: " + id + " currency: " + currency);
				
				long hashInterval = 30000;// default
				
				// start db configuration cache --> refresh every one hour
				if (timeToWaitCoinMarketCap.isEmpty()) {
					hashInterval = EnvironmentConfigManager.getTickerHashIntervalCoinMarketCap();
					timeToWaitCoinMarketCap.put(LocalDateTime.now(), hashInterval);
					log.debug("First load from DB with interval : " +  hashInterval);
				} else {
					Iterator<Map.Entry<LocalDateTime, Long>> entries = timeToWaitCoinMarketCap.entrySet().iterator();
					if (entries.hasNext()) {
						Map.Entry<LocalDateTime, Long> entry = entries.next();
						if (entry.getKey().plusHours(ONE_HOUR).isAfter(LocalDateTime.now())) {
							hashInterval = entry.getValue();
						} else { // one hour is passed
							timeToWaitCoinMarketCap.clear();
							hashInterval = EnvironmentConfigManager.getTickerHashIntervalCoinMarketCap();
							timeToWaitCoinMarketCap.put(LocalDateTime.now(), hashInterval);
							log.debug("One hour is passed! Load from DB with interval : " +  hashInterval);
						}
					}
				}
				
				if (cacheTickerCoinMarketCap.containsKey(key) && (cacheTickerCoinMarketCap.get(key).getTimeCalled() + hashInterval) > serviceCallTime) {
		
					return cacheTickerCoinMarketCap.get(key);
		
				} else {
		
					String jsonResponse = Util.getResponseRestEasy(url);
		
					log.debug("Coinmarketcap response for ticker: " + jsonResponse);
		
					ticker.setRequestURL(url);
					ticker.setResponse(jsonResponse);
					
					ObjectMapper objectMapper = new ObjectMapper();
					JsonNode marketsResponse = objectMapper.readTree(jsonResponse);
					Iterator<JsonNode> iterator = marketsResponse.elements();
					if(iterator.hasNext()) {
						JsonNode el = iterator.next();
						ticker.setId(el.get("id").isNull() ? null : el.get("id").asText());
						ticker.setName(el.get("name").isNull() ? null : el.get("name").asText());
						ticker.setSymbol(el.get("symbol").isNull() ? null : el.get("symbol").asText());
						ticker.setRank(el.get("symbol").isNull() ? 0 : el.get("symbol").asInt());
						ticker.setPriceUSD(el.get("price_usd").isNull() ? null : el.get("price_usd").asDouble());
						ticker.setPriceBTC(el.get("price_btc").isNull() ? null : el.get("price_btc").asDouble());
						ticker.setDailyVolumeUSD(el.get("24h_volume_usd").isNull() ? null : el.get("24h_volume_usd").asDouble());
						ticker.setMarketCapUSD(el.get("market_cap_usd").isNull() ? null : el.get("market_cap_usd").asDouble());
						ticker.setAvailableSupply(el.get("available_supply").isNull() ? null : el.get("available_supply").asDouble());
						ticker.setTotalSupply(el.get("total_supply").isNull() ? null : el.get("total_supply").asDouble());
						ticker.setMaxSupply(el.get("max_supply").isNull() ? null : el.get("max_supply").asDouble());
						ticker.setHourPrecentChange(el.get("percent_change_1h").isNull() ? null : el.get("percent_change_1h").asDouble());
						ticker.setDayPrecentChange(el.get("percent_change_24h").isNull() ? null : el.get("percent_change_24h").asDouble());
						ticker.setWeekPercentChange(el.get("percent_change_7d").isNull() ? null : el.get("percent_change_7d").asDouble());
						ticker.setLastUpdated(el.get("last_updated").isNull() ? null : el.get("last_updated").asLong());
						ticker.setPriceCurrencyRequested(el.get(priceCurrencyRequestedFieldName).isNull() ? null : el.get(priceCurrencyRequestedFieldName).asDouble());
						ticker.setTimeCalled(serviceCallTime);
						cacheTickerCoinMarketCap.put(key, ticker);
						
					}
					
					return ticker;
				}
			}
				ticker.setResponse("Coinmarketcap.com does not support currency: " + currency);
				ticker.setRequestURL(url);
			return ticker;
			
	}

}
