package com.invest.crypto.bittrex.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invest.crypto.bittrex.config.EnvironmentConfigManager;
import com.invest.crypto.bittrex.model.marketapi.OpenOrder;
import com.invest.crypto.bittrex.model.marketapi.OrderCreated;
import com.invest.crypto.bittrex.util.Util;
import com.invest.crypto.common.security.SignatureUtil;

@Path("/marketapi")
public class BittrexMarketAPI {
	
	private static final Logger log = LogManager.getLogger(BittrexMarketAPI.class);

//	@GET
//	@Path("/buylimit")
//	@Produces(MediaType.APPLICATION_JSON)
	public OrderCreated buyLimit(/*@QueryParam("market")*/ String market,/*@QueryParam("quantity")*/ BigDecimal quantity, /*@QueryParam("quantity")*/ BigDecimal rate) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SQLException {
		
		log.debug("buylimit is called with params -->  market: " + market + "quantity: " + quantity + "rate: " + rate);
		
		String nonce = SignatureUtil.createNonce();
		
		String baseUrl = EnvironmentConfigManager.getBaseUrl().get(EnvironmentConfigManager.URL);
		
		String url = baseUrl + "/market/buylimit?market=" + market + "&apikey=" + Util.API_KEY + "&quantity=" + quantity + "&rate=" + rate + "&nonce=" + nonce;
		
		String requestURL = "buylimit URL: " + baseUrl + "/market/buylimit?market=" + market + "&quantity=" + quantity + "&rate=" + rate + "&nonce=" + nonce + "&API_KEY";
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponseRestEasy(url, sign);
		
		log.debug("Bittrex response for buyLimit: " + jsonResponse);
		
		OrderCreated orderCreated = new OrderCreated();
		orderCreated.setResponse(jsonResponse);
		orderCreated.setRequestURL(requestURL);
			
			
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode marketsResponse = objectMapper.readTree(jsonResponse);
			String success = marketsResponse.get("success").asText();
			if (success.equals("true")) {
				JsonNode result = marketsResponse.get("result");
				if(!result.isNull()) {
					orderCreated.setUuid(result.get("uuid").isNull() ? null : result.get("uuid").asText());
				}
			}

		return orderCreated;
	}
	
//	@GET
//	@Path("/selllimit")
//	@Produces(MediaType.APPLICATION_JSON) 
	public OrderCreated sellLimit(String market, BigDecimal quantity, BigDecimal rate) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SQLException {
		
		log.debug("selllimit is called with params --> market: " + market + "quantity: " + quantity + "rate: " + rate);
		
		String nonce = SignatureUtil.createNonce();
		
		String baseUrl = EnvironmentConfigManager.getBaseUrl().get(EnvironmentConfigManager.URL);
		
		String url = baseUrl + "/market/selllimit?market=" + market + "&apikey=" + Util.API_KEY + "&quantity=" + quantity + "&rate=" + rate + "&nonce=" + nonce;
		
		String requestURL = "sellLimit URL: " + baseUrl + "/market/selllimit?market=" + market + "&quantity=" + quantity + "&rate=" + rate + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);
		
		String jsonResponse = Util.getResponseRestEasy(url, sign);
		
		log.debug("Bittrex response for sellLimit: " + jsonResponse);
		
		OrderCreated orderCreated = new OrderCreated();
		
		orderCreated.setResponse(jsonResponse);
		orderCreated.setRequestURL(requestURL);
			
			ObjectMapper objectMapper = new ObjectMapper();

			JsonNode marketsResponse = objectMapper.readTree(jsonResponse);
			String success = marketsResponse.get("success").asText();
			if (success.equals("true")) {
				JsonNode result = marketsResponse.get("result");
				if(!result.isNull()) {
					orderCreated.setUuid(result.get("uuid").isNull() ? null : result.get("uuid").asText());
				}
			}
			
		return orderCreated;
	}
	
//	@GET
//	@Path("/getopenorders")
//	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<OpenOrder> getOpenOrders(String market) throws JsonProcessingException, IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SQLException {
		
		log.debug("getopenorders is called with params --> market: " + market);
		
		String nonce = SignatureUtil.createNonce();
		String baseUrl = EnvironmentConfigManager.getBaseUrl().get(EnvironmentConfigManager.URL);
		
		String url = baseUrl + "/market/getopenorders?market=" + market + "&apikey=" + Util.API_KEY + "&nonce=" + nonce;
		
		String requestURL = "getopenorders URL: " + baseUrl + "/market/getopenorders?market=" + market + "&nonce=" + nonce + "&API_KEY";
		
		log.debug(requestURL);
		
		String sign = SignatureUtil.calculateSignature(url);

		String jsonResponse = Util.getResponseRestEasy(url, sign);
		
		log.debug("Bittrex response for getopenorders: " + jsonResponse);
		
		OpenOrder openOrderResponse = new OpenOrder();
		openOrderResponse.setResponse(jsonResponse);
		openOrderResponse.setRequestURL(requestURL);
		
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayList<OpenOrder> openOrders = new ArrayList<OpenOrder>();
		openOrders.add(openOrderResponse);

			JsonNode openOrdersResponse = objectMapper.readTree(jsonResponse);
			String success = openOrdersResponse.get("success").asText();
			if (success.equals("true")) {
				JsonNode result = openOrdersResponse.get("result");
				if(!result.isNull()) {
					if (result.isArray()) {
						for (JsonNode o : result) {
							OpenOrder openOrder = new OpenOrder();
							openOrder.setUuid(o.get("uuid").isNull() ? null : o.get("uuid").asText());
							openOrder.setOrderUuid(o.get("OrderUuid").isNull() ? null : o.get("OrderUuid").asText());
							openOrder.setExchange(o.get("Exchange").isNull() ? null : o.get("Exchange").asText());
							openOrder.setOrderType(o.get("OrderType").isNull() ? null : o.get("OrderType").asText());
							openOrder.setQuantity(o.get("Quantity").isNull() ? null : o.get("Quantity").decimalValue());
							openOrder.setQuantityRemaining(o.get("QuantityRemaining").isNull() ? null : o.get("QuantityRemaining").decimalValue());
							openOrder.setLimit(o.get("Limit").isNull() ? null : o.get("Limit").decimalValue());
							openOrder.setCommissionPaid(o.get("CommissionPaid").isNull() ? null : o.get("CommissionPaid").decimalValue());
							openOrder.setPrice(o.get("Price").isNull() ? null : o.get("Price").decimalValue());
							openOrder.setPricePerUnit(o.get("PricePerUnit").isNull() ? null : o.get("PricePerUnit").decimalValue());
							openOrder.setOpened(o.get("Opened").isNull() ? null : LocalDateTime.parse(o.get("Opened").asText()));
							openOrder.setClosed(o.get("Closed").isNull() ? null : LocalDateTime.parse(o.get("Closed").asText()));
							openOrder.setCancelInitiated(o.get("CancelInitiated").isNull() ? null : o.get("CancelInitiated").asBoolean());
							openOrder.setImmediateOrCancel(o.get("ImmediateOrCancel").isNull() ? null : o.get("ImmediateOrCancel").asBoolean());
							openOrder.setIsConditional(o.get("IsConditional").isNull() ? null : o.get("IsConditional").asBoolean());
							openOrder.setCondition(o.get("Condition").isNull() ? null : o.get("Condition").asText());
							openOrder.setConditionTarget(o.get("ConditionTarget").isNull() ? null : o.get("ConditionTarget").asText());
							openOrders.add(openOrder);
						}
					}
				}
			}

		return openOrders;
	}
}
