/**
 * 
 */
package com.invest.crypto.bittrex.util;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

//import org.jboss.resteasy.client.jaxrs.ResteasyClient;
//import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
//import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

/**
 * @author Tabakov
 *
 */
public class Util {

	public static final String HEADER_KEY = "apisign";
	// TODO these should be removed
	public static final String API_KEY = "2b33c084cc274742b916596f16e7fee1";
	public static final String API_KEY_WITHDRAW = "c8406539054f48fbad6908940f4557e2";

	public static String getResponseRestEasy(String url, String value) {

		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(url);
		String response = target.request(MediaType.APPLICATION_JSON).header(HEADER_KEY, value).get(String.class);
		return response;
	}

	public static String getResponseRestEasy(String url) {

		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(url);
		String response = target.request(MediaType.APPLICATION_JSON).get(String.class);
		return response;

	}

	public static boolean isNull(String value) {
		if (value.equals("null")) {
			return true;
		} else {
			return false;
		}
	}

}
