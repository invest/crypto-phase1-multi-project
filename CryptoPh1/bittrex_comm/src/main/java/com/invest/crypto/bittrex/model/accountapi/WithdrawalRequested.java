package com.invest.crypto.bittrex.model.accountapi;

import com.invest.crypto.bittrex.model.common.Response;

/**
 * @author Tabakov
 */
public class WithdrawalRequested extends Response{

	private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

	@Override
	public String toString() {
		return "WithdrawalRequested [uuid=" + uuid + "]";
	}
}
