package com.invest.crypto.bittrex.model.publicapi;

import java.math.BigDecimal;

import com.invest.crypto.bittrex.model.common.Response;

/**
 * @author Tabakov
 */
public class OrderBookEntry extends Response{

    private BigDecimal Quantity;
    private BigDecimal Rate;

    public BigDecimal getQuantity() {
        return Quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        Quantity = quantity;
    }

    public BigDecimal getRate() {
        return Rate;
    }

    public void setRate(BigDecimal rate) {
        Rate = rate;
    }

	@Override
	public String toString() {
		return "OrderBookEntry [Quantity=" + Quantity + ", Rate=" + Rate + "]";
	}
    
}