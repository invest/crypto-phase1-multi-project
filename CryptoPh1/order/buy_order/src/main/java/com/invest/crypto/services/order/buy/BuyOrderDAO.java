package com.invest.crypto.services.order.buy;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.invest.crypto.services.order.OrderDAO;
import com.invest.crypto.services.order.OrderManager.OrderState;

/**
 * @author Kiril.m
 */
class BuyOrderDAO extends OrderDAO {

	static List<BuyOrder> getOrders(Connection con/*, int stateId*/) throws SQLException {
		String sql = "select id, market, original_amount, fee, clearance_fee, rate as currency_rate, "
						+ "(original_amount - fee - ifnull(clearance_fee, 0)) * rate as base_amount_without_fees "
					+ "from crypto_trade where status = " + OrderState.READY_FOR_PROCESSING.getId();
		List<BuyOrder> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
//			ps.setInt(1, stateId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BuyOrder order = new BuyOrder(	rs.getLong("id"), rs.getString("market"), rs.getBigDecimal("original_amount"),
												rs.getBigDecimal("base_amount_without_fees"), rs.getBigDecimal("fee"),
												rs.getBigDecimal("clearance_fee"), rs.getBigDecimal("currency_rate"));
				result.add(order);
			}
		}
		return result;
	}

	static boolean lockOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_trade set status = " + OrderState.LOCKED.getId()
					+ " where status = " + OrderState.READY_FOR_PROCESSING.getId() + " and id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean unlockOrder(Connection con, long orderId, String orderUuid, BigDecimal quantity, BigDecimal rate, BigDecimal bittrex_commission) throws SQLException {
		String sql = "update crypto_trade set status = " + OrderState.PROCESSED.getId() + ", uuid = ?, time_opened = now(), quantity = ?, bittrex_commission = ?, "
						+ "rate_market = ? "
					+ "where id = ? and status = " + OrderState.LOCKED.getId();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, orderUuid);
			ps.setBigDecimal(2, quantity.subtract(bittrex_commission, new MathContext(6, RoundingMode.HALF_UP)));
			ps.setBigDecimal(3, bittrex_commission);
			ps.setBigDecimal(4, rate);
			ps.setLong(5, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean updateFailedOrder(Connection con, long orderId, BigDecimal quantity, BigDecimal rate) throws SQLException {
		String sql = "update crypto_trade set status = " + OrderState.PROCESSING_FAILED.getId() + ", quantity = ?, rate_market = ? "
					+ "where id = ? and status = " + OrderState.LOCKED.getId();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setBigDecimal(1, quantity);
			ps.setBigDecimal(2, rate);
			ps.setLong(3, orderId);
			return ps.executeUpdate() == 1;
		}
	}
	
	public static boolean isBuyLimitSkipped(Connection con) throws SQLException {
		String sql = "select config_value from crypto_config where config_key = '" + BuyOrderManager.SKIP_BUY_LIMIT + "'";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getBoolean("config_value");
			}
		}
		return false;
	}
	
	public static HashMap<String, BigDecimal> getBittrexCommissions(Connection con) throws SQLException {
		HashMap<String, BigDecimal> bcMap = new HashMap<>();
		String sql = "SELECT crypto_currency, commission FROM bittrex_commissions";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				bcMap.put(rs.getString("crypto_currency"), rs.getBigDecimal("commission"));
			}
		}
		return bcMap;
	}
}