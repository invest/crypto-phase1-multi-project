package com.invest.crypto.services.order.buy;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.model.marketapi.OrderCreated;
import com.invest.crypto.bittrex.model.publicapi.OrderBookEntry;
import com.invest.crypto.bittrex.model.publicapi.Ticker;
import com.invest.crypto.common.email.EmailUtil;
import com.invest.crypto.services.order.OrderManager;

/**
 * @author Kiril.m
 */
class BuyOrderManager extends OrderManager {

	private static final Logger log = LogManager.getLogger(BuyOrderManager.class);
	static final String SKIP_BUY_LIMIT = "SKIP_BITREX_CONFIRMATION";

	static List<BuyOrder> getOrders(/* int stateId */) {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.getOrders(con/* , stateId */);
		} catch (SQLException e) {
			log.debug("Unable to load orders in state 10", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	static boolean lockOrder(long orderId) {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.lockOrder(con, orderId);
		} catch (SQLException e) {
			log.debug("Unable to lock order with id " + orderId, e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	static void unlockOrder(long orderId, String orderUuid, BigDecimal quantity, BigDecimal rate, BigDecimal commission) {
		Connection con = null;
		try {
			con = getConnection();
			if (!BuyOrderDAO.unlockOrder(con, orderId, orderUuid, quantity, rate, commission)) {
				log.warn("Unlock order update in the db did not execute as expected");
			}
		} catch (SQLException e) {
			log.debug("Unable to unlock order with id " + orderId, e);
		} finally {
			closeConnection(con);
		}
	}

	static BuyOrder placeBuyLimit(String apiKey, BuyOrder order/* String market, BigDecimal amount */, boolean skip_buy_limit) {
		try {
			Ticker ticker = getPublicApi().getTicker(order.getMarketName());
			BigDecimal rate = ticker.getAsk();
			BigDecimal quantity = order.getBaseAmountWithoutFees().divide(rate, 8, RoundingMode.HALF_UP);
			order.setQuantity(quantity);
			order.setRate(rate);
			// TODO apiKey should be used
			if (!skip_buy_limit) {
				// rate need to be set based on orderbook call
				ArrayList<OrderBookEntry> orderBookSellList = getPublicApi().getOrderBookForSell(order.getMarketName());
				if (orderBookSellList.isEmpty()) {
					log.debug("orderBookSellList is empty, rate is set from ticker call.......");
				} else {
					BigDecimal quantityTmp = new BigDecimal(0);
					BigDecimal one = new BigDecimal(1);
					for (OrderBookEntry obe : orderBookSellList) {
						quantityTmp = quantityTmp.add(obe.getQuantity());
						if (quantity.add(one).compareTo(quantityTmp) < 0) {
							order.setRate(obe.getRate());
							log.debug("order rate is set based on orderBookSellList accumulated value, which is: " + obe.getRate());
							break;
						}
					}
				}

				OrderCreated orderCreated = getMarketApi().buyLimit(/* apiKey, */order.getMarketName(), quantity, rate);
				if (orderCreated != null) {
					order.setUuid(orderCreated.getUuid());
				}
			} else {
				log.debug("About to generate random UUID.......");
				String uuid = UUID.randomUUID().toString();
				order.setUuid(uuid);
			}
			
			return order;
		} catch (IOException e) {
			log.debug("Unable to place buy limit order. Returning null", e);
			EmailUtil.sendAlertEmail(	"com.invest.crypto.services.order.buy.BuyOrderManager.placeBuyLimit(String, BuyOrder) exception",
										"Unable to place buy limit order.\n" + e);
			return null;
		} catch (InvalidKeyException | NoSuchAlgorithmException | IllegalStateException | SQLException e) {
			log.debug("Unable to make buy limit request. Returning null", e);
			EmailUtil.sendAlertEmail(	"com.invest.crypto.services.order.buy.BuyOrderManager.placeBuyLimit(String, BuyOrder) exception",
										"Unable to make buy limit request.\n" + e);
			return null;
		}
	}

	static void updateFailedOrder(long orderId, BigDecimal quantity, BigDecimal rate) {
		Connection con = null;
		try {
			con = getConnection();
			if (!BuyOrderDAO.updateFailedOrder(con, orderId, quantity, rate)) {
				log.warn("Update of failed order did not execute as expected");
			}
		} catch (SQLException e) {
			log.debug("Unable to update failed order with id " + orderId, e);
		} finally {
			closeConnection(con);
		}
	}
	
	public static Boolean isBuyLimitSkipped() {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.isBuyLimitSkipped(con);
		} catch (SQLException e) {
			log.debug("Unable to get buylimit flag due to: ", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
	
	public static HashMap<String, BigDecimal> getBittrexCommissions() {
		Connection con = null;
		try {
			con = getConnection();
			return BuyOrderDAO.getBittrexCommissions(con);
		} catch (SQLException e) {
			log.debug("Unable to get bittrex commissions due to: ", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
}