package com.invest.crypto.services.order.buy;

import java.math.BigDecimal;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Kiril.m
 */
class BuyOrderTask implements Runnable {

	private static final Logger log = LogManager.getLogger(BuyOrderTask.class);
	private String apiKey;
	private BuyOrder buyOrder;

	public BuyOrderTask(String apiKey, BuyOrder buyOrder) {
		this.apiKey = apiKey;
		this.buyOrder = buyOrder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		log.debug("Starting buy order task for " + buyOrder);
		Boolean skipBuyLimit = BuyOrderManager.isBuyLimitSkipped();
		if (skipBuyLimit != null) {
			if (BuyOrderManager.lockOrder(buyOrder.getId())) {
				BuyOrder createdOrder = BuyOrderManager.placeBuyLimit(apiKey, buyOrder, skipBuyLimit);
				if (createdOrder != null && createdOrder.getUuid() != null) {
					// start retreive bittrex commission
					BigDecimal commission = new BigDecimal(0);
					try {
						HashMap<String, BigDecimal> commissionList = BuyOrderManager.getBittrexCommissions();
						String[] pairs = buyOrder.getMarketName().split("-");
						if (!commissionList.isEmpty() && commissionList.containsKey(pairs[1])) {
							commission = commissionList.get(pairs[1]);
						} else {
							log.debug("Bittrex commission can not be retrieved from DB, so 0 (zero) is set insted. ");
						}
						
					} catch (Exception e) {
						log.debug("Bittrex commission can not be retreived, NULL is inserted!");
					}
					
					BuyOrderManager.unlockOrder(buyOrder.getId(), createdOrder.getUuid(), createdOrder.getQuantity(), createdOrder.getRate(), commission);
				} else {
					BuyOrderManager.updateFailedOrder(buyOrder.getId(), buyOrder.getQuantity(), buyOrder.getRate());
				}
			} else {
				log.debug("The order could not be locked. Probably it's already locked");
			}
			log.debug("Task completed for buy order with id " + buyOrder.getId());
		} else {
			log.debug("Task is skipped, because skipBuyLimit flag is null");
		}
	}
}