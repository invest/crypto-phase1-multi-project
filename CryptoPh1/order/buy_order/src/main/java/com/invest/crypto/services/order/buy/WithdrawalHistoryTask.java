package com.invest.crypto.services.order.buy;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.services.order.buy.WithdrawalHistoryManager.WithdrawalHistoryCurrency;

/**
 * @author Kiril.m
 */
class WithdrawalHistoryTask implements Runnable {

	private static final Logger log = LogManager.getLogger(WithdrawalHistoryTask.class);
	private String apiKey;
	private WithdrawalHistoryCurrency currency;

	public WithdrawalHistoryTask(String apiKey, WithdrawalHistoryCurrency currency) {
		this.apiKey = apiKey;
		this.currency = currency;
	}

	@Override
	public void run() {
		log.debug("Starting withdrawal history order task for currency " + currency);
		Set<WithdrawalHistory> dbHistory = WithdrawalHistoryManager.getWithdrawalHistoryFromDb(currency);
		Set<WithdrawalHistory> newHistory = WithdrawalHistoryManager.getWithdrawalHistoryFromApi(apiKey, currency);
		newHistory.removeAll(dbHistory);
		if (newHistory.size() > 0) {
			WithdrawalHistoryManager.insertNewWithdrawalHistory(newHistory);
		} else {
			log.debug("There is nothing to insert");
		}
		log.debug("Task completed");
	}
}