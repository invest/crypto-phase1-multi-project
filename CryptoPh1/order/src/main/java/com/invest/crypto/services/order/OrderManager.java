package com.invest.crypto.services.order;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.invest.crypto.bittrex.api.BittrexAccountAPI;
import com.invest.crypto.bittrex.api.BittrexMarketAPI;
import com.invest.crypto.bittrex.api.BittrexPublicAPI;
import com.invest.crypto.common.util.ManagerBase;

/**
 * @author Kiril.m
 */
public abstract class OrderManager extends ManagerBase {

	private static final Logger log = LogManager.getLogger(OrderManager.class);
	private static BittrexAccountAPI accountApi;
	private static BittrexMarketAPI marketApi;
	private static BittrexPublicAPI publicApi;

	public enum OrderConfigKey {
		API_KEY, SCHEDULED_PERIOD, TASK_COUNT;
	}

	public enum OrderState {
							READY_FOR_MANUAL_PROCESSING(1), READY_FOR_PROCESSING(10), LOCKED(20), PROCESSING_FAILED(-20), PROCESSED(30), CLOSED(40);

		private final int id;
		private static final Map<Integer, OrderState> ID_STATE_MAP = new HashMap<>(OrderState.values().length);
		static {
			for (OrderState state : OrderState.values()) {
				ID_STATE_MAP.put(state.getId(), state);
			}
		}

		private OrderState(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

		public static OrderState getById(int id) {
			OrderState state = ID_STATE_MAP.get(id);
			if (state == null) {
				throw new IllegalArgumentException("No order state with id: " + id);
			} else {
				return state;
			}
		}
	}

	public static OrderConfig getOrderConfig() {
		Connection con = null;
		try {
			con = getConnection();
			return OrderDAO.getOrderConfig(con);
		} catch (SQLException e) {
			log.debug("Unable to load config, returning null", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	public static BittrexAccountAPI getAccountApi() {
		if (accountApi == null) {
			accountApi = new BittrexAccountAPI();
		}
		return accountApi;
	}

	public static BittrexMarketAPI getMarketApi() {
		if (marketApi == null) {
			marketApi = new BittrexMarketAPI();
		}
		return marketApi;
	}

	public static BittrexPublicAPI getPublicApi() {
		if (publicApi == null) {
			publicApi = new BittrexPublicAPI();
		}
		return publicApi;
	}
}