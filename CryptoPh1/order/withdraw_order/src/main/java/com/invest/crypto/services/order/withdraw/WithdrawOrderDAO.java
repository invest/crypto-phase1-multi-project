package com.invest.crypto.services.order.withdraw;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.invest.crypto.services.order.OrderDAO;
import com.invest.crypto.services.order.OrderManager.OrderState;

/**
 * @author Kiril.m
 */
class WithdrawOrderDAO extends OrderDAO {

	static List<WithdrawOrder> getOrders(Connection con/* , int stateId */) throws SQLException {
		String sql = "select id, currency, quantity + ifnull(bittrex_commission,0) as quantity, wallet from crypto_withdraw where status = " + OrderState.READY_FOR_PROCESSING.getId();
		List<WithdrawOrder> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			// ps.setInt(1, stateId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				WithdrawOrder order = new WithdrawOrder(rs.getLong("id"), rs.getString("currency"), rs.getBigDecimal("quantity"),
														rs.getString("wallet"));
				result.add(order);
			}
		}
		return result;
	}

	static boolean lockOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_withdraw set status = " + OrderState.LOCKED.getId()
					+ " where status = " + OrderState.READY_FOR_PROCESSING.getId() + " and id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean unlockOrder(Connection con, long orderId, String orderUuid) throws SQLException {
		String sql = "update crypto_withdraw set status = " + OrderState.PROCESSED.getId() + ", uuid = ?, time_updated = now() "
					+ "where id = ? and status = " + OrderState.LOCKED.getId();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, orderUuid);
			ps.setLong(2, orderId);
			return ps.executeUpdate() == 1;
		}
	}

	static boolean updateFailedOrder(Connection con, long orderId) throws SQLException {
		String sql = "update crypto_withdraw set status = " + OrderState.PROCESSING_FAILED.getId() + ", time_updated = now() "
					+ "where id = ? and status = " + OrderState.LOCKED.getId();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, orderId);
			return ps.executeUpdate() == 1;
		}
	}
}