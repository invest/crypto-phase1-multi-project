package com.invest.crypto.common.email;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.invest.crypto.common.email.EmailManager.EmailParams;

/**
 * @author Kiril.m
 */
class EmailDAO {

	static EmailConfig getConfig(Connection con) throws SQLException {
		String sql = "select config_key, config_value from crypto_config where config_key in (?, ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, EmailParams.SMTP_HOST.toString());
			ps.setString(2, EmailParams.RECIPIENTS.toString());
			ResultSet rs = ps.executeQuery();
			String host = null;
			String recipients = null;
			while (rs.next()) {
				String value = rs.getString("config_value");
				switch (EmailParams.valueOf(rs.getString("config_key"))) {
				case SMTP_HOST:
					host = value;
					break;

				case RECIPIENTS:
					recipients = value;

				default:
					break;
				}
			}
			return new EmailConfig(host, recipients);
		}
	}
}