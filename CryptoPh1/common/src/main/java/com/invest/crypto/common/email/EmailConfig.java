package com.invest.crypto.common.email;

/**
 * @author Kiril.m
 */
class EmailConfig {

	private String sMTPHost;
	private String recipients;
	
	EmailConfig(String sMTPHost, String recipients) {
		this.sMTPHost = sMTPHost;
		this.recipients = recipients;
	}

	public String getSMTPHost() {
		return sMTPHost;
	}
	
	public void setSMTPHost(String sMTPHost) {
		this.sMTPHost = sMTPHost;
	}
	
	public String getRecipients() {
		return recipients;
	}
	
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getClass().getName() + ls
				+ super.toString() + ls
				+ "sMTPHost: " + sMTPHost + ls
				+ "recipients: " + recipients + ls;
	}
}